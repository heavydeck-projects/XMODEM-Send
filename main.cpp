/* Copyright (C) 2020 J.Luis <root@heavydeck.net>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 */
#include "mainwindow.h"
#include <QApplication>
#include <QTranslator>
#include <QLocale>
#include <QDir>
#include <QSettings>
#include <QIcon>
#include <QDebug>

#include "app-info.h"

#ifdef WIN32
#include <windows.h>
static void startup_quirks(){
    //Remove the DOS window that pops up on startup for some reason.
    //It used not to be an issue with GCC builds but now it is...
    FreeConsole();
}
#else
static void startup_quirks(){
}
#endif

int main(int argc, char *argv[])
{
#ifdef QT_DEBUG
    //Debug options, reduce the ammount of magic, make debugging faster.
    QApplication::setAttribute(Qt::AA_UseSoftwareOpenGL, true);
    QApplication::setAttribute(Qt::AA_DontUseNativeDialogs, true);
    QApplication::setAttribute(Qt::AA_DontUseNativeMenuBar, true);
#else
    //Release options
    //(nothing)
#endif
    startup_quirks();

    QApplication a(argc, argv);
    a.setApplicationName(APPLICATION_NAME);
    a.setOrganizationName(ORGANIZATION_NAME);
    a.setOrganizationDomain(ORGANIZATION_DOMAIN);
    a.setApplicationVersion(
                QString::number(VERSION_MAJOR) + "." +
                QString::number(VERSION_MINOR) + "." +
                QString::number(VERSION_BUILD)
                );

    //Translation
    QTranslator myappTranslator;
    {
        QSettings settings;
        QString locale = settings.value(KEY_LANGUAGE, QLocale::system().name()).toString();
        if(locale != ""){
            //Try loading localizations first from resources, then from external files
            bool rv;
            rv = myappTranslator.load("xmodem-send_" + locale, ":/translations");
            if(!rv)
            {
                qDebug() << "Failed to add Locale" << locale << "from resource, trying external file";
                rv = myappTranslator.load("xmodem-send_" + locale, QDir::currentPath() + "/translations");
            }
            if (rv){
                qDebug() << "Added locale for:" << locale;
                a.installTranslator(&myappTranslator);
            }
            else{
                qDebug() << "Failed to add locale for:" << locale << "Current path is:" << QDir::currentPath();
            }
        }
    }

    MainWindow w;
    w.setWindowIcon(QIcon("://icon/vga.ico"));
    w.show();

    return a.exec();
}
