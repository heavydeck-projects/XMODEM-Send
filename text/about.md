(C) 2024 heavydeck.net;
This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License version 2.

The Qt framework is released under the GNU Lesser General Public
License version 3. Visit www.qt.io for more information.

Icon by 'Those Icons' visit www.flaticon.com for more information.
